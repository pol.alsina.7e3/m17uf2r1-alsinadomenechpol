using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Inventory", menuName = "ScriptableObjects/Inventory", order = 3)]
public class Inventory : ScriptableObject
{
    public WeaponsData[] weapons = new WeaponsData[5];
    public WeaponsData weapon;
    public void ClearInventory()
    {
        //weapons = new WeaponsData[5];
        for (int i = 0; i < weapons.Length; i++)
        {
            weapons[i] = null;
        }
    }
    public void InventoryStart()
    {
        ClearInventory();
        weapon.isIniatilize();
        weapons[0] = weapon;
    }

}
