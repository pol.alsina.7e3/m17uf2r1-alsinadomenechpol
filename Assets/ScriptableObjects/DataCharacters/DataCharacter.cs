using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEditor.Progress;

[CreateAssetMenu(fileName = "DataCharacter", menuName = "ScriptableObjects/DataCharacter", order = 1)]
public class DataCharacter : ScriptableObject
{
   [SerializeField] private new string name;
    [SerializeField] private string description;
    [SerializeField] private int maxHealth;
    [SerializeField] private int damage;
    public int currentHealth;
    public  string Name { get => name; set => name = value; }
    public string Description { get => description; set => description = value; }
    public int MaxHealth { get => maxHealth; set => maxHealth = value; }
    public int Damage { get => damage; set => damage = value; }

 }
