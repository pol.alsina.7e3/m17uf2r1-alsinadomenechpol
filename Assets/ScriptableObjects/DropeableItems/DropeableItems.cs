using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "DropeableItems", menuName = "ScriptableObjects/DropeableItems", order = 4)]
public class DropeableItems : ScriptableObject
{
    public GameObject[] dropItems;
    public int[] probItems;
   public GameObject ItemDrop(int index)
    {
        if (index <= probItems[0])
        {
            return dropItems[0];
        }
        else if (index <= probItems[1])
        {
            return dropItems[1];
        }
        else
        {
            return dropItems[2];
        }
    }
}
