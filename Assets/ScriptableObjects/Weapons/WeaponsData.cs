using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "WeaponsData", menuName = "ScriptableObjects/WeaponsData", order = 2)]
public class WeaponsData : ScriptableObject
{
    public new string name;
    public string description;
    public Sprite spriteWeapon;
    public int bulletForEachCharger;
    public int charger;
    public float timeToRecharge;
    public int maxBullets;
    public int currentBullets;
    public int costToBuy;
    public float shootSpeed;


    [Header("Bullet")]
    public int damage;
    public float bulletSpead;
    public GameObject goBullet;
    public float maxDistance;

    public float BulletSpead { get => bulletSpead; set => bulletSpead = value; }
    public GameObject GOBulllet { get => goBullet; set => goBullet = value; }

    public void ClearBullets()
    {
        currentBullets = 0;
        maxBullets = 0;
    }

    public void isIniatilize()
    {
       maxBullets = bulletForEachCharger * charger;
       currentBullets = bulletForEachCharger; 
    }
}
