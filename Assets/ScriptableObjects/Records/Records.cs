using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Records", menuName = "ScriptableObjects/Records", order = 5)]
public class Records : ScriptableObject
{
    public List<int> lastScores;
    public int bestScore;

    public void CheckNewScoreRecord(int score)
    {
        if (bestScore < score) bestScore = score;
    }
    public void AddListScore(int score)
    {
        CheckList();
        lastScores.Add(score);
    }
    public void CheckList()
    {
        int lenghtList = lastScores.Capacity;
        if (lenghtList >= 5)
        {
            lastScores.RemoveAt(0);
        }
    }
}
