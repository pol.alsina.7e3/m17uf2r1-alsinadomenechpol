using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "EconomyPlayerData", menuName = "ScriptableObjects/EconomyPlayerData", order = 5)]
public class EconomyPlayerData : ScriptableObject
{

    public int coinsYellow;
    public int coinsRed;

    public void ResetCoinsYellow()
    {
        coinsYellow = 0;
    }
}
