using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Coins : MonoBehaviour
{
    public EconomyPlayerData playerData;
    public Text coinsRed;
    public Text coinsYellow;
    PickCoins pickCoins;
    void Start()
    {
        GameManager.updateUICoins += UpdateUICoins;
        UpdateUICoins();
    }
    void UpdateUICoins()
    {
        coinsRed.text = playerData.coinsRed.ToString();
        coinsYellow.text = playerData.coinsYellow.ToString();
    }
    private void OnDisable()
    {
        GameManager.updateUICoins -= UpdateUICoins;
    }
}
