using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIManagerMenu : MonoBehaviour
{
    public void PlayButton()
    {
        SceneManager.LoadScene("LobbyScene");
    }
    public void ExitButton()
    {
        Application.Quit();
    }
}
