using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CursorChangeSkin : MonoBehaviour
{
    public Texture2D cursor;

    // Update is called once per frame
    public void ChangeCursor()
    {
        Vector2 cursorPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Cursor.SetCursor(cursor, cursorPos,CursorMode.Auto);        
        //transform.position = cursorPos;
    }
}
