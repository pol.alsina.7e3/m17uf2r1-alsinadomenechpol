using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponsDropCollide : MonoBehaviour
{
    public WeaponsData[] weaponsData;
    public Inventory inventory;
    public int bulletsdrop;
    private SpriteRenderer _sR;
    private int index;
    private PickWeapon pickWeapon;

    private void Start()
    {
        index = Random.Range(0, weaponsData.Length);
        _sR = GetComponent<SpriteRenderer>();
        _sR.sprite = weaponsData[index].spriteWeapon;
        pickWeapon = new PickWeapon();
        pickWeapon.inventory = inventory;
        pickWeapon.weaponData = weaponsData[index];
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            pickWeapon.CheckToPickWeapon();
            Destroy(this.gameObject);
        }
    }
}
