using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireSkull : Enemy
{

    private float time;
    public float intervalToShoot;
    public GameObject bullet;
    public float bulletSpeed;

    public override void Start()
    {
        base.Start();
    }
    public override void Update()
    {
        Animation();
        GetDistanceToPlayer();
        time += Time.deltaTime;
        if (time > intervalToShoot && canMove && !isStunned)
        {
            Shoot();
            time = 0;
        }
    }
    private void Shoot()
    {
        
        GameObject tempBullet = Instantiate(bullet, new Vector3(transform.position.x, transform.position.y, 0), transform.rotation) as GameObject;
        Rigidbody2D tempRigidBodyBullet = tempBullet.GetComponent<Rigidbody2D>();
        tempRigidBodyBullet.AddForce(direction * bulletSpeed);
        Destroy(tempBullet, 10f);
    }
    public override void AplyDamage(int damageTaken)
    {
        base.AplyDamage(damageTaken);
    }
}
