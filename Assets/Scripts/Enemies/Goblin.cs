using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Goblin : Enemy
{
    private float offSet;
    public override void Start()
    {
        base.Start();
    }
    public override void Update()
    {
        GetDistanceToPlayer();
        //float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;

        if (distance < distanceToFollowPlayer && canMove && !isStunned)
        {
            transform.position = Vector2.MoveTowards(this.transform.position, player.transform.position, speed * Time.deltaTime);
        }
        if (distance > offSet)
        {
            animator.SetBool("Attack", true);
        }
        else
        {
            animator.SetBool("Attack", false);
        }
        Animation();
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {

        IDamageble damageble = collision.gameObject.GetComponent<IDamageble>();
        if (damageble != null)
        {
            if (collision.gameObject.CompareTag("Player"))
            {
                damageble.AplyDamage(enemydata.Damage);
                GameManager.instance.economyData.coinsYellow -= RandomStealCoins();
                GameManager.instance.UpdateCoins();
                Destroy(this.gameObject);
            }
        }
    }
    int RandomStealCoins()
    {
        return Random.Range(10, 50);
    }
}
