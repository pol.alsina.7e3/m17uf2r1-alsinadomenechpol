using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class Ghost : Enemy 
{
    
    public override void Start()
    {
        base.Start();
    }
    public override void Update()
    {
        base.Update();
        Animation();
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        
        IDamageble damageble = collision.gameObject.GetComponent<IDamageble>();
        if (damageble != null)
        {
            if (collision.gameObject.CompareTag("Player"))
            {
                damageble.AplyDamage(enemydata.Damage);
                //GameManager.instance.enemyToDefeat--;
                Destroy(this.gameObject);
            }                    
        }
    }
    public override void AplyDamage(int damageTaken)
    {
        base.AplyDamage(damageTaken);
    }
}
