using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemy : MonoBehaviour, IDamageble, IDropable, IStuneable, IBurned
{
    public float speed;
    public GameObject player;
   [SerializeField] protected int currentHealth;
    protected float distance;
    public float distanceToFollowPlayer;
    protected Vector2 direction;
    public DataCharacter enemydata;
    public HealthBar healthBar;
    protected Animator animator;
    protected Collider2D colliderEnemy;
    protected bool canMove = true;
    public DropeableItems items;
    private AudioSource audioSource;
    [SerializeField] AudioClip deathClipAudio;
    public GameObject particles;
    public Text textStatusEnemy;
    
    
    public int damage { get; set; }

    public DropeableItems dropeableItems { get => items; }
    public bool isStunned { get; set ; }

    public virtual void Start()
    {
        colliderEnemy = GetComponent<Collider2D>();
        animator = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
        player = GameObject.Find("Player");
        currentHealth = enemydata.MaxHealth;
        healthBar.SetMaxHealt(enemydata.MaxHealth);
        particles.SetActive(false);
    }
    public virtual void Update()
    {
        GetDistanceToPlayer();
        //float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;

        if (distance < distanceToFollowPlayer && canMove && !isStunned)
        {
            transform.position = Vector2.MoveTowards(this.transform.position, player.transform.position, speed * Time.deltaTime);
            //transform.rotation = Quaternion.Euler(Vector3.forward * angle);
        }
    }
    protected void GetDistanceToPlayer()
    {
        distance = Vector2.Distance(transform.position, player.transform.position);
        direction = player.transform.position - transform.position;
        direction.Normalize();
    }
    protected void CheckHealth()
    {
        if (currentHealth <= 0)
        {
            //GameManager.instance.enemyToDefeat--;
            //GameManager.instance.enemyDefeted++;
            audioSource.PlayOneShot(deathClipAudio);
            canMove = false;
            animator.SetBool("Death", true);
            Destroy(colliderEnemy);            
        }
    }

    public virtual void AplyDamage(int damageTaken)
    {
        currentHealth -= damageTaken;
        healthBar.SetHealth(currentHealth);
        CheckHealth();
    }
    public void Destroy()
    {
        DropItem();
        Destroy(this.gameObject);
    }
    protected void Animation()
    {
        animator.SetFloat("Horizontal", player.transform.position.x - transform.position.x);
        animator.SetFloat("Vertical", player.transform.position.y - transform.position.y);
    }

    public void DropItem()
    {
        int itemDrop = Random.Range(0, 100);
        Instantiate(dropeableItems.ItemDrop(itemDrop), this.transform.position,Quaternion.identity);
    }

    public void TimeIsStunned(float stunnedTime)
    {
        isStunned=true;
        StartCoroutine(Stunned(stunnedTime));
    }
    IEnumerator Stunned(float stunnedTime)
    {
        particles.SetActive(true);
        textStatusEnemy.text = "Stunned";
        animator.enabled = false;
        yield return new WaitForSeconds(stunnedTime);
        particles.SetActive(false);
        textStatusEnemy.text = "";
        animator.enabled = true;
        isStunned = false;
    }
    protected void OnDestroy()
    {
        GameManager.instance.enemyToDefeat--;
        GameManager.instance.enemyDefeted++;
    }

    public void ImBurned(int damageContinued, float timeBurned)
    {
        textStatusEnemy.text = "Burned";
        currentHealth -= damageContinued;
        healthBar.SetHealth(currentHealth);
        CheckHealth();
        textStatusEnemy.text = "";
        
    }
}
