using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DashManaHUD : MonoBehaviour
{
    public Slider dashManaBar;
    public Image fill;
    public void SetMaxDashMana(int dashMana)
    {
        dashManaBar.maxValue = dashMana;
        dashManaBar.value = dashMana;       
    }

    public void SetDashMana(int dashMana)
    {
        dashManaBar.value = dashMana;
    }
}
