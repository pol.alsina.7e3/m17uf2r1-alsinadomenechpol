using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class PauseHUD : MonoBehaviour
{
    public GameObject pausePannel;
    public GameObject optionsPannel;
    public GameObject controlsPannel;
    private bool isPause = false;

    private void Update()
    {
        if (!isPause && Input.GetKey(KeyCode.Q))
        {
            PauseOn();
        }
        if (isPause && Input.GetKey(KeyCode.Q))
        {
            PauseOff();
        }
    }

    public void PauseOn()
    {
        Time.timeScale = 0f;
        pausePannel.SetActive(true);
        isPause = true;
    }
    public void PauseOff()
    {
        Time.timeScale = 1f;
        pausePannel.SetActive(false);
        isPause = false;
    }
    public void MenuButton()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene("MenuScene");
    }
    public void RetreatButton()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene("LobbyScene");
    }
    public void ExitButton()
    {
        Application.Quit();
    }
    public void ControlsButton()
    {
        controlsPannel.SetActive(true);
        pausePannel.SetActive(false);
    }
    public void OptionsButton()
    {
        optionsPannel.SetActive(true);
        pausePannel.SetActive(false);
    }
}
