using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.Universal;
using UnityEngine.UI;

public class HitBloodUI : MonoBehaviour
{
    public Sprite bloodLight;
    public Sprite bloodMedium;
    public Sprite bloodHardcore;
    private Image image;
 
    private void Start()
    {
        image = GetComponent<Image>();
        PlayerHealth.hitBloodUI += CheckTakeHitBloodUI;
        RestoreHealth.updateHealthBar += LowHPBloodUIDesactive;
        var tempColor = image.color;
        tempColor.a = 0f;
        image.color = tempColor;
    }
    public void CheckTakeHitBloodUI(int health)
    {
        if (health >= 50) StartCoroutine(ShowBloodHitUI(bloodLight));  
        else if (health >= 30) StartCoroutine(ShowBloodHitUI(bloodMedium));
        else LowHPBloodUIActive();
    }
    IEnumerator ShowBloodHitUI(Sprite blood)
    {
        image.sprite = blood;
        var tempColor = image.color;
        tempColor.a = 1f;
        image.color = tempColor;
        //DegrateAlpha();
        yield return new WaitForSeconds(2f);
        tempColor.a = 0f;
        image.color = tempColor;
    }
    private void OnDisable()
    {
        PlayerHealth.hitBloodUI -= CheckTakeHitBloodUI;
        RestoreHealth.updateHealthBar -= LowHPBloodUIDesactive;
    }
    void LowHPBloodUIActive()
    {
        //MusicManager.PitchMusic();
        image.sprite = bloodHardcore;
        var tempColor = image.color;
        tempColor.a = 1f;
        image.color = tempColor;      
    }
    void LowHPBloodUIDesactive()
    {
        image.sprite = bloodHardcore;
        var tempColor = image.color;
        tempColor.a = 0f;
        image.color = tempColor;
        //MusicManager.DespitchMusic();
    }
}
