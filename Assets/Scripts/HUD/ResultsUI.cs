using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ResultsUI : MonoBehaviour
{
    private int score;
    public Text text;
    public Text lastScores;
    public Text bestScore;
    public Records records;
    private void Start()
    {
        if(SceneManager.GetActiveScene().name == "GameScene")
        {
            CalculatePoints();
        }
        else
        {
            ShowResults();
        }
    }
    public void PlayButton()
    {
        SceneManager.LoadScene("GameScene");
        ResetVariableGameManager();
    }
    public void MenuButton()
    {
        SceneManager.LoadScene("MenuScene");
        ResetVariableGameManager();
    }
    void CalculatePoints()
    {
        score = GameManager.instance.CalculatePoints();
        records.CheckNewScoreRecord(score);        
        records.AddListScore(score);
        ShowResults();
    }
    void ShowResults()
    {
        if (text != null) text.text = "Score: " + score.ToString();
        foreach (var record in records.lastScores)
        {
            lastScores.text += record.ToString() + "\n";
        }
        bestScore.text = records.bestScore.ToString();
    }
    void ResetVariableGameManager()
    {
        GameManager.instance.ResetPoints();
        GameManager.instance.RoomsCleared = 0;
        GameManager.instance.enemyDefeted = 0;
    }
}
