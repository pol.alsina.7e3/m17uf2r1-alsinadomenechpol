using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class InventoryUI : MonoBehaviour
{
    public Inventory inventory;
    public Image[] slots;
    public GameObject[] indexWeapon;
    //SUBSCRIBE EVENT
    // Start is called before the first frame update
    void Start()
    {
        UpdateInventoryUI();
        PickWeapon.updateUIWeapons += UpdateInventoryUI;
        Weapon.updateUIWeaponsIndex += IndexUIWeapon;
    }
    private void OnDisable()
    {
        Weapon.updateUIWeaponsIndex -= IndexUIWeapon;
        PickWeapon.updateUIWeapons -= UpdateInventoryUI;
    }
    private void UpdateInventoryUI()
    {
        for (int i = 0; i < inventory.weapons.Length; i++)
        {
            if (inventory.weapons[i] != null)
            {
                slots[i].sprite = inventory.weapons[i].spriteWeapon;
                slots[i].enabled = true; 
            }
            
        }
    }
    private void IndexUIWeapon(int index)
    {
        DeactivateIndexUI();
        indexWeapon[index].SetActive(true);

    }
    void DeactivateIndexUI()
    {
        for (int i = 0; i < indexWeapon.Length; i++)
        {
            indexWeapon[i].SetActive(false);
        }
    }
    
}
