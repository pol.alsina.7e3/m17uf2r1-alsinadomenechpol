using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerOneEnemy : Spawner
{
    int index;
    // Start is called before the first frame update
    void Start()
    {
        minEnemySpawn = 3;
        maxEnemySpawn = 5;
        CalculateEnemiesToSpawn();
        index = EnemySpawnIndex();
    }

    // Update is called once per frame
    void Update()
    {
        time += Time.deltaTime;

        if (time > interval && enemiesMaxToSpawn != 0)
        {
            Instantiate(enemiesToSpawn[index], spawnPoints[GenerateRandomIndexSpawnPoints()].transform.position, Quaternion.identity);
            enemiesMaxToSpawn--;
            time = 0;
            Debug.Log(enemiesMaxToSpawn);
        }
        CheckEnemyToDefeat();
    }
}
