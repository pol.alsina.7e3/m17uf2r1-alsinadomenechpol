using UnityEngine;


public class Spawner : MonoBehaviour
{
    public GameObject[] enemiesToSpawn;
    public GameObject[] spawnPoints;
    protected int minEnemySpawn, maxEnemySpawn;
    protected int enemiesMaxToSpawn;
    protected float interval = 1f;
    protected float time;
    public delegate void OpenDoor();// event to update ui of weapons in game
    public static event OpenDoor openDoor;
    private void Start()
    {
        minEnemySpawn = 3;
        maxEnemySpawn = 5;
        CalculateEnemiesToSpawn();
    }
    void Update()
    {
        time += Time.deltaTime;

        if (time > interval && enemiesMaxToSpawn != 0)
        {
            Instantiate(enemiesToSpawn[EnemySpawnIndex()], spawnPoints[GenerateRandomIndexSpawnPoints()].transform.position, Quaternion.identity);
            enemiesMaxToSpawn--;
            time = 0;
            Debug.Log(enemiesMaxToSpawn);
        }
        CheckEnemyToDefeat();
    }
    protected int GenerateRandomIndexSpawnPoints()
    {
        return Random.Range(0, spawnPoints.Length);
    }
    protected int EnemySpawnIndex()
    {
        return Random.Range(0, enemiesToSpawn.Length);
    }
    protected void CalculateEnemiesToSpawn()
    {
        minEnemySpawn += GameManager.instance.RoomsCleared; 
        maxEnemySpawn += GameManager.instance.RoomsCleared;
        enemiesMaxToSpawn = Random.Range(minEnemySpawn, maxEnemySpawn);
        GameManager.instance.enemyToDefeat = enemiesMaxToSpawn;
        Debug.Log(GameManager.instance.enemyToDefeat);
    }
    protected void CheckEnemyToDefeat()
    {
        if (GameManager.instance.enemyToDefeat <= 0)
        {
            if (openDoor != null) openDoor();
        }
    }
}

