using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootBullet : MonoBehaviour
{
    public GameObject bullet;
    private float bulletSpeed = 3000f;
    Vector3 lookPos;

    // Update is called once per frame
    void Update()
    {
        Vector3 mousePos = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0);
        lookPos = Camera.main.ScreenToWorldPoint(mousePos)- transform.position;

        if (Input.GetMouseButtonDown(0))
        {
            Shoot();
        }

    }
    private void Shoot()
    {
        
        GameObject tempBullet = Instantiate(bullet, new Vector3(transform.position.x, transform.position.y, 0), transform.rotation) as GameObject;
        Rigidbody2D tempRigidBodyBullet = tempBullet.GetComponent<Rigidbody2D>();
        tempRigidBodyBullet.AddForce(lookPos.normalized * bulletSpeed);
        Destroy(tempBullet, 10f);
    }
}
