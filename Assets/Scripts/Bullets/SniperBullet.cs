using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SniperBullet : BulletBehavior
{
    private int _hitsBullet;
    // Update is called once per frame
    void Update()
    {
        DistanceBullet();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Wall"))
        {
            Destroy(this.gameObject);
        }
        IDamageble damageble = collision.gameObject.GetComponent<IDamageble>();
        if (damageble != null && !collision.gameObject.CompareTag("Player"))
        {
            damageble.AplyDamage(weaponData.damage);
            _hitsBullet++;
            CheckHits();
            
        }
    }
    void CheckHits()
    {
        if (_hitsBullet >= 2)
        {
            Destroy(this.gameObject);
        }
    }
}

