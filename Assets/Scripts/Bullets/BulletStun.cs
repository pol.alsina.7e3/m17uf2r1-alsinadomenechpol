using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletStun : BulletBehavior
{

    private void Update()
    {
        DistanceBullet();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Wall"))
        {
            Destroy(this.gameObject);
        }
        IStuneable stuneable = collision.gameObject.GetComponent<IStuneable>();
        if (stuneable != null && !collision.gameObject.CompareTag("Player"))
        {
            stuneable.TimeIsStunned(TimeStunnedRandom());
            Destroy(this.gameObject);
        }
    }
    float  TimeStunnedRandom()
    {
        return Random.Range(5f, 8.5f);
    }
}
