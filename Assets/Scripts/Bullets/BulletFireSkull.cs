using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletFireSkull : MonoBehaviour
{
    public DataCharacter enemyData;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        IDamageble damageble = collision.gameObject.GetComponent<IDamageble>();
        if (damageble != null)
        {
            if (collision.gameObject.CompareTag("Player"))
            {
                damageble.AplyDamage(enemyData.Damage);
                Destroy(this.gameObject);
            }
        }
        if (collision.gameObject.CompareTag("Wall"))
        {
            Destroy(this.gameObject);
        }
    }
}
