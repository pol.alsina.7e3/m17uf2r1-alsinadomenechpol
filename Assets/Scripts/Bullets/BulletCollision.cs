using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEditor.Progress;

public class BulletCollision : MonoBehaviour
{
    public WeaponsData weaponData;
    public float speed;
    private float _currentDistance;
    private void Update()
    {
        _currentDistance += weaponData.bulletSpead * Time.deltaTime;

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Wall"))
        {            
            Destroy(this.gameObject);
        }
        IDamageble damageble = collision.gameObject.GetComponent<IDamageble>();
        if (damageble != null && !collision.gameObject.CompareTag("Player"))
        {
           damageble.AplyDamage(weaponData.damage);
           Destroy(this.gameObject);  
        }
    }
}

