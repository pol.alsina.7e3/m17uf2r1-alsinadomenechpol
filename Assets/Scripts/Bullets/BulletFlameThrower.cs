using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletFlameThrower : BulletBehavior
{
    void Update()
    {
        DistanceBullet();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Wall"))
        {
            Destroy(this.gameObject);
        }
        IBurned burned = collision.GetComponent<IBurned>();
        if (burned != null && !collision.gameObject.CompareTag("Player"))
        {
            burned.ImBurned(weaponData.damage,3f);
            Destroy(gameObject.GetComponent<Collider2D>());
        }
    }
}


