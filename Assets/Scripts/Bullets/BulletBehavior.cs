using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletBehavior : MonoBehaviour
{
    public WeaponsData weaponData;
    public Vector3 initialPosition;
    private float _currentDistance;
    void Update()
    {
        DistanceBullet();
    }
    protected void DistanceBullet()
    {
        //_currentDistance += weaponData.bulletSpead * Time.deltaTime;
        _currentDistance = Vector3.Distance(initialPosition, transform.position);
        if (_currentDistance >= weaponData.maxDistance)
        {
            Destroy(gameObject);
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Wall"))
        {
            Destroy(this.gameObject);
        }
        IDamageble damageble = collision.gameObject.GetComponent<IDamageble>();
        if (damageble != null && !collision.gameObject.CompareTag("Player"))
        {
            damageble.AplyDamage(weaponData.damage);
            Destroy(this.gameObject);
        }
    }
}

