using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class BoxDestroy : MonoBehaviour , IDamageble, IDropable, IBurned
{
    private SpriteRenderer _sR;
    private Collider2D _sCollision;
    private int healt;
    public DropeableItems items;
    public ParticleSystem particleSystem;
    private bool _isBurned = false;
    public DropeableItems dropeableItems => items;
    private void Awake()
    {
        _sCollision = GetComponent<Collider2D>();
        _sR = GetComponent<SpriteRenderer>();
        particleSystem.Pause();    
    }
    public int damage { get; set; } 

    public void AplyDamage(int damage)
    {
        DestroyBox();       
    }

    public void DropItem()
    {
        int itemDrop = Random.Range(0, 100);
        Instantiate(dropeableItems.ItemDrop(itemDrop), this.transform.position, Quaternion.identity);
    }
    void DestroyBox()
    {
        gameObject.transform.GetChild(0).gameObject.SetActive(false);
        DropItem();
        Destroy(_sCollision);
        Destroy(_sR);
    }
    public void ImBurned(int damageContinued, float timeBurned)
    {
        if (!_isBurned) StartCoroutine(BurnBox(damageContinued, timeBurned));
    }
    IEnumerator BurnBox(int damageContinued, float timeBurned)
    {
        _isBurned = true;
        particleSystem.Play();
        yield return new WaitForSeconds(timeBurned);
        particleSystem.Stop();
        DestroyBox();
    }
}
 