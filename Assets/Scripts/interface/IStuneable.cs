using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IStuneable
{
    public bool isStunned { get; set; } // bool to denied the enemy movment

    void TimeIsStunned(float StunnedTime); // That method will calculate the time the enemy will be stunned

}
