using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDropable 
{
    public DropeableItems dropeableItems { get; }
    void DropItem();
}
