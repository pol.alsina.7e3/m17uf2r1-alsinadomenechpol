using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalEndBehaviour : MonoBehaviour
{
    //private bool _isOpen;
    private AudioSource audioSource;
    public GameObject winPannelResults;
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        GameManager.instance.open = false;
        Spawner.openDoor += Open;
    }

    void Open()
    {
        Time.timeScale = 0f;
        AudioManager.instance.RoomClearPlay(audioSource);
        GameManager.instance.open = false;
        winPannelResults.SetActive(true);
        Spawner.openDoor -= Open;
    }
    private void OnDisable()
    {
        Spawner.openDoor -= Open;
    }


}
