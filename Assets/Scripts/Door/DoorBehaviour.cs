using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DoorBehaviour : MonoBehaviour
{
    private Spawner spawner;
    private SpriteRenderer _sRUp;
    //private bool _isOpen;
    [SerializeField] SpriteRenderer _sRDown;
    public Sprite doorUp;
    public Sprite doorDown;
    public GameObject pannelE;
    public Text textE;
    private Collider2D _collider2D;
    public delegate void NewRoom();
    public static event NewRoom newRoom;
    private AudioSource audioSource;
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        _sRUp = this.GetComponent<SpriteRenderer>();
        _collider2D = this.GetComponent<Collider2D>();
        GameManager.instance.open = false;
        Spawner.openDoor += Open;
    }

    void Open()
    {
        AudioManager.instance.RoomClearPlay(audioSource);
        GameManager.instance.open = false;
        //_isOpen = true;
        _sRUp.sprite = doorUp;
        _sRDown.sprite = doorDown;
       if(_collider2D!=null) _collider2D.enabled = true;
        //AudioManager.instance.RoomClearPlay();
        Spawner.openDoor -= Open;
    }
    private void OnDisable()
    {
        Spawner.openDoor -= Open;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {         
            textE.text = "Next Room";
            pannelE.SetActive(true);
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player") && Input.GetKey(KeyCode.E))
        {
            Debug.Log("I'm trying to change the room");
            Destroy(this.gameObject.GetComponent<Collider2D>());
            StartCoroutine(ChangeRoom());
            
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        pannelE.SetActive(false);
    }
    IEnumerator ChangeRoom()
    {
        GameManager.instance.RoomsCleared++;
        int index = Random.Range(0, GameManager.instance.maps.Length);
        if (GameManager.instance.RoomsCleared > 9)
        {
            Instantiate(GameManager.instance.lastMap, new Vector3(transform.position.x, transform.parent.position.y + 50f, transform.position.z), Quaternion.identity);
        }
        else if (GameManager.instance.RoomsCleared > 4)
        {
            Instantiate(GameManager.instance.shopMap, new Vector3(transform.position.x, transform.parent.position.y + 50f, transform.position.z), Quaternion.identity);           
        }
        else
        {
            Instantiate(GameManager.instance.maps[index], new Vector3(transform.position.x, transform.parent.position.y + 50f, transform.position.z), Quaternion.identity);
        }
        newRoom();
        yield return new WaitForSeconds(0.1f);
        Destroy(transform.parent.gameObject);
        //fadeout
    }
}
