using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransportPlayerNewRoom : MonoBehaviour
{
    private void Awake()
    {
        DoorBehaviour.newRoom += TPplayer;
    }
    void TPplayer()
    {
        GameObject player = GameObject.Find("Player");
        player.transform.position = this.transform.position;
        DoorBehaviour.newRoom -= TPplayer;
    }
    private void OnDisable()
    {
        DoorBehaviour.newRoom -= TPplayer;
    }
}
