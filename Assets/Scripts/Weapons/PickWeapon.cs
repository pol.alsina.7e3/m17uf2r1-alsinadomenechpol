
public class PickWeapon
{
    public Inventory inventory;
    public WeaponsData weaponData;
    private bool isWeaponOnInventory = false;
    private bool nowIsOnInventory = false;
    public delegate void UpdateUIWeapons();// event to update ui of weapons in game
    public static event UpdateUIWeapons updateUIWeapons;
    public delegate void UpdateWeaponsSpriteAndData();
    public static event UpdateWeaponsSpriteAndData updateWeaponsSpriteAndData;
    void CheckInventory()
    {
        for (int i = 0; i < inventory.weapons.Length; i++)
        {
            if (weaponData == inventory.weapons[i])
            {
                weaponData.maxBullets += BulletsToPick();
                isWeaponOnInventory = true;
            }
        }
    }
    void TakeWeapon()
    {
        for (int i = 0; i < inventory.weapons.Length; i++)
        {
            if (inventory.weapons[i] == null && !nowIsOnInventory)
            {
                inventory.weapons[i] = weaponData;
                inventory.weapons[i].isIniatilize();
                nowIsOnInventory = true;
                if (updateUIWeapons != null) updateUIWeapons();
            }
        }
    }
    void ChangeWeapon()
    {
        inventory.weapons[Weapon.inventoryIndex] = weaponData;
        if (updateUIWeapons != null) updateUIWeapons();
        if (updateWeaponsSpriteAndData != null) updateWeaponsSpriteAndData();
    }
    public void CheckToPickWeapon()
    {
        CheckInventory();
        if (!isWeaponOnInventory) TakeWeapon();
        if (!nowIsOnInventory && !isWeaponOnInventory) ChangeWeapon();
    }
    int BulletsToPick()
    {
        return weaponData.bulletForEachCharger / 2;
    }
}
