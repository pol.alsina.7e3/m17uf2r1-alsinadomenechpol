using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;

public class Weapon : MonoBehaviour
{
    public WeaponsData weaponData;
    protected float time;
    protected float timeWeapon;
    Vector3 lookPos;
    public Text textBullets;
    public Text textRecharge;
    public Inventory inventory;
    public static int inventoryIndex = 0;
    bool canRecharge;
    public delegate void UpdateUIWeaponsIndex(int index);// event to update ui of weapons in game
    public static event UpdateUIWeaponsIndex updateUIWeaponsIndex;

    private void Awake()
    {
        inventoryIndex = 0;
        inventory.InventoryStart();// clear inventory and puts a default weapon
        inventory.weapons[inventoryIndex] = inventory.weapon;
        weaponData = inventory.weapons[inventoryIndex];
        gameObject.GetComponent<SpriteRenderer>().sprite = weaponData.spriteWeapon;
        PickWeapon.updateWeaponsSpriteAndData += UpdateWeaponSpriteAndData;
        timeWeapon = weaponData.shootSpeed;
    }
    // Update is called once per frame
    void Update()
    {
        Vector3 mousePos = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0);
        lookPos = Camera.main.ScreenToWorldPoint(mousePos) - transform.position;
        timeWeapon += Time.deltaTime;
        if (InputManager.instance.GetShootInput() && weaponData.currentBullets != 0 && timeWeapon > weaponData.shootSpeed)
        {          
            Shoot();
            timeWeapon = 0;
        }
        ChangeWeapon();
    }

    private void LateUpdate()
    {
        canRecharge = weaponData.currentBullets == 0 && weaponData.maxBullets != 0;
        if (InputManager.instance.GetRechargeInput() || canRecharge ) ReloadWeapon();
        textBullets.text = weaponData.currentBullets + " | " + weaponData.maxBullets; //Updates the text for player see how many bullets have
    }

    private void Shoot()
    {
        GameObject tempBullet = Instantiate(weaponData.GOBulllet, new Vector3(transform.position.x, transform.position.y, 0), transform.rotation) as GameObject;
        Rigidbody2D tempRigidBodyBullet = tempBullet.GetComponent<Rigidbody2D>();
        tempRigidBodyBullet.velocity = lookPos.normalized * weaponData.BulletSpead;
        tempBullet.GetComponent<BulletBehavior>().initialPosition = new Vector3(transform.position.x, transform.position.y, 0);
        tempBullet.GetComponent<BulletBehavior>().weaponData = weaponData;
        weaponData.currentBullets--;
        // tempRigidBodyBullet.AddForce(lookPos.normalized * weaponData.BulletSpead);
    }
    private void ChangeWeapon()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {            
            inventoryIndex++;         
            CheckIndex();
            if (updateUIWeaponsIndex != null) updateUIWeaponsIndex(inventoryIndex);
            UpdateWeaponSpriteAndData();
            /*weaponData = inventory.weapons[inventoryIndex];           
            gameObject.GetComponent<SpriteRenderer>().sprite = weaponData.spriteWeapon;*/
        }
    }
    private void CheckIndex()
    {
        if (inventoryIndex > 4)
        {
            inventoryIndex = 0;

        }
        else if (inventory.weapons[inventoryIndex] == null)
        {
            inventoryIndex = 0;
        }  
    }
    void ReloadWeapon()
    {
            time += Time.deltaTime;
            textRecharge.text = "Recharging...";
            if (weaponData.timeToRecharge < time)
            {
                int bulletsToRest = weaponData.bulletForEachCharger - weaponData.currentBullets;
                weaponData.currentBullets = bulletsToRest; //Put the charger the bullets to its normal
                weaponData.maxBullets -= bulletsToRest;//subtratck the max bullets the weapon has 
                time = 0;
                textRecharge.text = " ";
             }  
    }
    /*IEnumerator RechargeWeapon()
    {
        canShoot = false;
        textRecharge.text = "Recharging...";
        yield return new WaitForSeconds(weaponData.timeToRecharge);
        int bulletsToRest = weaponData.bulletForEachCharger - weaponData.currentBullets;
        weaponData.currentBullets += bulletsToRest; //Put the charger the bullets to its normal
        weaponData.maxBullets -= bulletsToRest;//subtratck the max bullets the weapon has 
        textRecharge.text = " ";
        canShoot = true;
    }*/

    public void UpdateWeaponSpriteAndData()
    {
        weaponData = inventory.weapons[inventoryIndex];
        gameObject.GetComponent<SpriteRenderer>().sprite = weaponData.spriteWeapon;
    }
    private void OnDisable()
    {
       PickWeapon.updateWeaponsSpriteAndData -= UpdateWeaponSpriteAndData;
    }
}
