using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuffPlayer : MonoBehaviour
{
    public DataCharacter playerData;
    public EconomyPlayerData economyData;
    public WeaponsData[] weaponsData;
    public DataCharacter[] enemies;
    public Text redCoins;

    public int costBuffHealth;
    public int costBuffDmageWeapons;
    public int costBuffMoreBullets;
    public int costBuffEnemies;

    public void BuffHealth()
    {
        if (economyData.coinsRed >= costBuffHealth)
        {
            playerData.MaxHealth += 10;
            economyData.coinsRed -= costBuffHealth;
            UpdateCoins();
        }        
    }

    public void BuffWeaponsDamage()
    {
        if (economyData.coinsRed >= costBuffDmageWeapons)
        {
            for (int i = 0; i < weaponsData.Length; i++)
            {
                weaponsData[i].damage += CalculateBuff(i, weaponsData[i].damage, 10);
            }
            economyData.coinsRed -= costBuffDmageWeapons; 
            UpdateCoins();
        }
    }
    public void BuffMoreBullets()
    {
        if (economyData.coinsRed >= costBuffMoreBullets)
        {
            for (int i = 0; i < weaponsData.Length; i++)
            {
                weaponsData[i].maxBullets += CalculateBuff(i, weaponsData[i].maxBullets,5);
            }
            economyData.coinsRed -= costBuffMoreBullets;
            UpdateCoins();
        }
    }

    public void BuffEnemiesLifes()
    {
        if (economyData.coinsRed >= costBuffEnemies)
        {
            for (int i = 0; i < enemies.Length; i++)
            {
                enemies[i].MaxHealth += CalculateBuff(i, enemies[i].MaxHealth,5);
            }
            economyData.coinsRed -= costBuffEnemies;
            UpdateCoins();
        }
    }

    public void BuffEnemiesDamage()
    {
        if (economyData.coinsRed >= costBuffEnemies)
        {
            for (int i = 0; i < enemies.Length; i++)
            {
                enemies[i].Damage += CalculateBuff(i, enemies[i].Damage, 5);
            }
            economyData.coinsRed -= costBuffEnemies;
            UpdateCoins();
        }
    }

    private int CalculateBuff(int index, int statToBuff, int percentatgeToLevelUp)
    {
        int buff = (statToBuff / 100) * percentatgeToLevelUp;
        Debug.Log(buff);
        return buff;
    }

    public void UpdateCoins()
    {
        redCoins.text = economyData.coinsRed.ToString();
    }
}
