using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static PickCoins;

public class PickCoins : MonoBehaviour
{
    public EconomyPlayerData economyData;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("CoinYellow"))
        {
            economyData.coinsYellow += AmountOfCoins(10,50);
            GameManager.instance.UpdateCoins();
            Destroy(collision.gameObject);
        }
        if (collision.gameObject.CompareTag("CoinRed"))
        {
            economyData.coinsRed += AmountOfCoins(1,10);
            GameManager.instance.UpdateCoins();
            Destroy(collision.gameObject);
        }
    }
    int AmountOfCoins(int min, int max)
    {
        return Random.Range(min, max);
    }
}
