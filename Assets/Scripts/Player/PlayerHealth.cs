using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using static PlayerHealth;

public class PlayerHealth : MonoBehaviour , IDamageble
{

    public DataCharacter playerData;
    public HealthBar healthBar;
    public GameObject pannelResults;
    private AudioSource audioSource;
    public delegate void HitBloodUIDmg(int health);
    public static event HitBloodUIDmg hitBloodUI;

    public int damage { get; set; }

    

    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 1f;
        playerData.currentHealth = playerData.MaxHealth;
        healthBar.SetMaxHealt(playerData.MaxHealth);
        RestoreHealth.updateHealthBar += UpdateUIHealth;
        audioSource = GetComponent<AudioSource>();
    }
    private void CheckHealth()
    {
        hitBloodUI(playerData.currentHealth);
        if (playerData.currentHealth <= 0)
        {
            MusicManager.StopMusic();
            AudioManager.instance.DeathSoundPlayer(audioSource);
            Time.timeScale = 0f;    
            pannelResults.SetActive(true);           
        }
    }
    public void AplyDamage(int damage)
    {    
        playerData.currentHealth -= damage;
        healthBar.SetHealth(playerData.currentHealth);
        CheckHealth();
    }
    void UpdateUIHealth()
    {
        healthBar.SetHealth(playerData.currentHealth);
    }
    private void OnDisable()
    {
        RestoreHealth.updateHealthBar -= UpdateUIHealth;
    }
    /* private void OnCollisionEnter2D(Collision2D collision)
     {
         Debug.Log(collision.gameObject.GetComponent<IDamageble>());
         IDamageble damageble = collision.gameObject.GetComponent<IDamageble>();
         if (damageble != null)
         {
             if (collision.gameObject.CompareTag("Enemy"))
             {
                 AplyDamage(damage);
             }

         }
     }*/
}
