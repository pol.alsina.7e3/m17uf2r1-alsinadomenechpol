using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSeeMouse : MonoBehaviour
{
    //SpriteRenderer _sprite;
    //GameObject player;
    private void Start()
    {
        //_sprite = GetComponent<SpriteRenderer>();
        //player = GameObject.Find("Player");
    }
    void Update()
    {
        Vector3 mousePos = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0);
        Vector3 lookPos = Camera.main.ScreenToWorldPoint(mousePos);
        lookPos -= transform.position;
        float angle = Mathf.Atan2(lookPos.y, lookPos.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        /*var mouse = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);
        var playerScreenPoint = Camera.main.WorldToScreenPoint(player.transform.position);

        if (mouse.x > playerScreenPoint.x)
        {
            _sprite.flipY = true;
        }
        else if (mouse.x < playerScreenPoint.x)
        {
            _sprite.flipY = false;
        } */  
    }
}
