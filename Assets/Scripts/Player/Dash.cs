﻿using System.Collections;
using Unity.VisualScripting;
using UnityEngine;

namespace Player
{
    public class Dash: MonoBehaviour
    {
        public float dashingPower;
        public float dashingTime;
        public TrailRenderer tr;
        public Rigidbody2D rB;
        public Animator animator;

        void Start()
        {
            tr = GetComponent<TrailRenderer>();
            rB = GetComponent<Rigidbody2D>();
            animator = GetComponent<Animator>();
        }        
        public IEnumerator DoDash(Vector2 position)
        {
            animator.SetTrigger("Roll");
            float originalGravity = rB.gravityScale;
            rB.gravityScale = 0f;
            rB.velocity = rB.position.normalized + position.normalized * dashingPower;
            tr.emitting = true;
            yield return new WaitForSeconds(dashingTime);
            rB.velocity = Vector2.zero;
            tr.emitting = false;
            rB.gravityScale = originalGravity;
            GameManager.instance.GetPlayer().GetComponent<MovePlayer>().enabled = true;
        }
    }
}