using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerShowSugestCollide : MonoBehaviour
{
    public GameObject pannelShowSugest;
    public GameObject pannelChangeCursor;
    public GameObject pannelBuffStats;
    public GameObject pannelRecords;
    public Text textSugest;
    public EconomyPlayerData playerData;
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Exit"))
        {
            textSugest.text = "Start Run";
            pannelShowSugest.SetActive(true);
        }
        if (other.gameObject.CompareTag("StoneBuffs"))
        {
            textSugest.text = "Buffs Your Stats ";
            pannelShowSugest.SetActive(true);
        }
        if (other.gameObject.CompareTag("StoneChangeCursor"))
        {
            textSugest.text = "Change Cursor ";
            pannelShowSugest.SetActive(true);
        }
        if (other.gameObject.CompareTag("AchivmentsStatue"))
        {
            textSugest.text = "Records";
            pannelShowSugest.SetActive(true);
        }
        if (other.gameObject.CompareTag("???"))
        {
            textSugest.text = "??? ";
            pannelShowSugest.SetActive(true);
        }
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Exit"))
        {
            if (Input.GetKey(KeyCode.E))
            {
                playerData.ResetCoinsYellow();
                SceneManager.LoadScene("GameScene");
            }
        }
        if (collision.gameObject.CompareTag("StoneBuffs"))
        {
            if (Input.GetKey(KeyCode.E))
            {
               pannelBuffStats.SetActive(true);
            }
        }
        if (collision.gameObject.CompareTag("StoneChangeCursor"))
        {
            if (Input.GetKey(KeyCode.E))
            {
                pannelChangeCursor.SetActive(true);
            }
        }
        if (collision.gameObject.CompareTag("AchivmentsStatue"))
        {
            if (Input.GetKey(KeyCode.E))
            {
                pannelRecords.SetActive(true);
            }
        }
    }
    private void OnTriggerExit2D(Collider2D other)
    {
      pannelShowSugest.SetActive(false);
    }
}
