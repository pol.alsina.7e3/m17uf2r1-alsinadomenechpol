using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class MovePlayerLobby : MonoBehaviour
{
    Rigidbody2D _rB;
    public float Speed = 5f;

    Animator animator;
    Vector2 _position;
    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 1f;
        _rB = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        _position.x = Input.GetAxis("Horizontal");
        _position.y = Input.GetAxis("Vertical");
        animator.SetFloat("Horizontal", _position.x);
        animator.SetFloat("Vertical", _position.y);
        animator.SetFloat("Speed", _position.sqrMagnitude);
    }
    void FixedUpdate()
    {
        _rB.MovePosition(_rB.position + _position.normalized * Speed * Time.fixedDeltaTime);
    }
}
