using System.Collections;
using System.Collections.Generic;
using Player;
using UnityEngine;
using UnityEngine.Serialization;

public class MovePlayer : MonoBehaviour
{
    Rigidbody2D _rB;
    public float speed;

    Animator _animator;
    Vector2 _position;
    
    public int maxDashMana;
    public int dashCost;
    private int _currentDashMana;
    public DashManaHUD dashMana;

    private float _time;
    public float intervalToRestoreDashMana;
    void Start()
    {
        _rB = GetComponent<Rigidbody2D>();
        _animator = GetComponent<Animator>();
        dashMana.SetMaxDashMana(maxDashMana);
        _currentDashMana = maxDashMana;
    }
    private void Update()
    {
        if (_currentDashMana <= maxDashMana)
        {
            ReFillDashMana();
        }
        if (InputManager.instance.GetDashInput() && _currentDashMana >= dashCost)
        {
            _currentDashMana -= dashCost;
            dashMana.SetDashMana(_currentDashMana);
            StartCoroutine(GetComponent<Dash>().DoDash(_position));
            this.enabled = false;
        }
    }
    void FixedUpdate()
    {
        ApplyAnimationsPlayer();
        ApplyMovePlayer();
    }

    void ApplyMovePlayer()
    {
        _position = InputManager.instance.GetMoveInput();
        _rB.MovePosition(_rB.position + _position.normalized * speed * Time.fixedDeltaTime);
    }

    void ApplyAnimationsPlayer()
    {
        _animator.SetFloat("Horizontal", _position.x);
        _animator.SetFloat("Vertical", _position.y);
        _animator.SetFloat("Speed", _position.sqrMagnitude);
    }

    void ReFillDashMana()
    {
        _time += Time.deltaTime;
        if (_time > intervalToRestoreDashMana)
        {
            _currentDashMana++;
            dashMana.SetDashMana(_currentDashMana);
            _time = 0;
        }
        
    }
   
}
