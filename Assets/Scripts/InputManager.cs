using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    public static InputManager instance;
    private MapInput _inputs;
    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this);
        }
        else
        {
            instance = this;
        }
        _inputs = new MapInput();
        EnableInputMapPlayer();
    }

    public void EnableInputMapPlayer()
    {
        _inputs.Player.Enable();
    }
    public void DisableInputMapPlayer()
    {
        _inputs.Player.Disable();
    }
    public Vector2 GetMoveInput()
    {
        return _inputs.Player.Move.ReadValue<Vector2>();
    }

    public bool GetItenratcInput()
    {
        return _inputs.Player.Interact.WasPerformedThisFrame();
    }

    public bool GetRechargeInput()
    {
        return _inputs.Player.Recharge.WasPerformedThisFrame();
    }

    public bool GetShootInput()
    {
        return _inputs.Player.Shoot.IsPressed();
    }

    public bool GetDashInput()
    {
        return _inputs.Player.Dash.WasPerformedThisFrame();
    }
}
