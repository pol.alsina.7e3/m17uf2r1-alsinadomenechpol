using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour
{
    public AudioClip[] musicClip;
    private static AudioSource musicSource;
    private static bool pause = false;
    void Awake()
    {
        pause = false;
        musicSource = GetComponent<AudioSource>();
        if (!musicSource.playOnAwake)
         {
             musicSource.clip = musicClip[Random.Range(0, musicClip.Length)];
             musicSource.Play();
         }       
    }


    void Update()
    {
        if (!musicSource.isPlaying && !pause)
        {
            musicSource.clip = musicClip[Random.Range(0, musicClip.Length)];
            musicSource.Play();
        }
    } 
    public static void PitchMusic()
    {
        musicSource.pitch = 1.5f;
    }
    public static void DespitchMusic()
    {
        musicSource.pitch = 1f;
    }
    public static void StopMusic()
    {
        pause = true;
        musicSource?.Stop();       
    }
    
}
