using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager instance;
    [SerializeField] AudioClip playerDeath;
    [SerializeField] AudioClip roomClearSound;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
    public void DeathSoundPlayer(AudioSource audioSource)
    {
        audioSource.PlayOneShot(playerDeath);
    }
    public void RoomClearPlay(AudioSource audioSource)
    {
        audioSource.PlayOneShot(roomClearSound);
    }
   
}
