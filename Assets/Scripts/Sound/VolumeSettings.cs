using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class VolumeSettings : MonoBehaviour
{
    public AudioMixer audioMixer;
    public Slider masterSlider;
    public Slider musicSlider;
    public Slider sfxSlider;

    const string mixerMaster = "Master";
    const string musicMaster = "MusicVolume";
    const string sfxMaster = "SFXVolume";

    private void Awake()
    {
        masterSlider.onValueChanged.AddListener(SetMasterVolume);
        musicSlider.onValueChanged.AddListener(SetMusicVolume);
        sfxSlider.onValueChanged.AddListener(SetSfxVolume);
    }
    void SetMasterVolume(float value)
    {
        audioMixer.SetFloat(mixerMaster, Mathf.Log10(value) * 20);
    }
    void SetMusicVolume(float value)
    {
        audioMixer.SetFloat(musicMaster, Mathf.Log10(value) * 20);
    }
    void SetSfxVolume(float value)
    {
        audioMixer.SetFloat(sfxMaster, Mathf.Log10(value) * 20);
    }
}
