using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RestoreHealth : MonoBehaviour
{
    private int toHealth;
    private int costHealth;
    public Text textInfoWhatItemDoes;
    public GameObject InfoCost;
    public GameObject toDestroyAfterBuy;
    private Text textInfoCost;
    public EconomyPlayerData economyPlayerData;
    public DataCharacter playerData;
    public GameObject pannelE;
    public Text textE;
    public delegate void UpdateHealthBar();
    public static event UpdateHealthBar updateHealthBar;
    // Start is called before the first frame update
    void Start()
    {
        textInfoCost = InfoCost.GetComponent<Text>();
        Debug.Log(playerData.currentHealth);
        toHealth = playerData.MaxHealth - playerData.currentHealth;
        CalculateCost();
        textInfoWhatItemDoes.text = toHealth + " Restore Health";
        textInfoCost.text = costHealth.ToString();
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            textE.text = "Buy";
            pannelE.SetActive(true);
        }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (economyPlayerData.coinsYellow >= costHealth && Input.GetKey(KeyCode.E))
        {
            playerData.currentHealth += toHealth;
            economyPlayerData.coinsYellow -= costHealth;
            updateHealthBar();
            //PlayerHealth.healthBar.SetHealth(playerData.currentHealth);
            GameManager.instance.UpdateCoins();
            Destroy(this.gameObject);
            Destroy(toDestroyAfterBuy);
        }
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        pannelE.SetActive(false);
    }
    private void CalculateCost()
    {
        if(playerData.currentHealth != playerData.MaxHealth)
        {
            costHealth = (playerData.MaxHealth - playerData.currentHealth);
        }
        else
        {
            costHealth = 0;
        }
        
    }
}
