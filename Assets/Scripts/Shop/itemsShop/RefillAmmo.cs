using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RefillAmmo : MonoBehaviour
{
    public EconomyPlayerData economyPlayerData;
    public Inventory Inventory;
    public int costToRefill;
    public GameObject InfoCost;
    public GameObject toDestroyAfterBuy;
    private Text textInfoCost;
    public Text textInfoWhatItemDoes;
    public GameObject pannelE;
    public Text textE;
    private void Start()
    {
        textInfoCost = InfoCost.GetComponent<Text>();
        textInfoCost.text = costToRefill.ToString();
        textInfoWhatItemDoes.text = "Refill all your ammo";
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            textE.text = "Buy";
            pannelE.SetActive(true);
        }
    }
    private void OnCollisionStay2D(Collision2D collision)
    {
        if (economyPlayerData.coinsYellow >= costToRefill && Input.GetKey(KeyCode.E))
        {
            for (int i = 0; i < Inventory.weapons.Length; i++)
            {
                if (Inventory.weapons[i] != null)
                {
                    Inventory.weapons[i].isIniatilize();
                }
            }
            economyPlayerData.coinsYellow -= costToRefill;
            GameManager.instance.UpdateCoins();
            Destroy(this.gameObject);
            Destroy(toDestroyAfterBuy);
        }
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        pannelE.SetActive(false);
    }
}
