using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static WeaponsDropCollide;

public class BuyWeapon : MonoBehaviour
{
    public Inventory inventory;
    public EconomyPlayerData economyPlayerData;
    public WeaponsData[] weaponsToBuy;
    public GameObject InfoCost;
    public GameObject toDestroyAfterBuy;
    private Text textInfoCost;
    public Text textInfoWhatItemDoes;
    private int index;
    private SpriteRenderer _sR;
    public GameObject pannelE;
    public Text textE;
    public PickWeapon pickWeapon;

    private void Start()
    {
        index = RandomIndex();
        //Sprite weapon
        _sR = GetComponent<SpriteRenderer>();
        _sR.sprite = weaponsToBuy[index].spriteWeapon;
        //Info of the weapon to buy
        textInfoCost = InfoCost.GetComponent<Text>();
        textInfoWhatItemDoes.text = "Buy Weapon " + weaponsToBuy[index].name ;
        textInfoCost.text = weaponsToBuy[index].costToBuy.ToString();
        pickWeapon = new PickWeapon();
        pickWeapon.inventory = inventory;
        pickWeapon.weaponData = weaponsToBuy[index];
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            textE.text = "Buy";
            pannelE.SetActive(true);
        }
    }
    private void OnCollisionStay2D(Collision2D collision)
    {
        if (economyPlayerData.coinsYellow >= weaponsToBuy[index].costToBuy && Input.GetKey(KeyCode.E))
        {
            economyPlayerData.coinsYellow -= weaponsToBuy[index].costToBuy;
            GameManager.instance.UpdateCoins();
            pickWeapon.CheckToPickWeapon();
            Destroy(toDestroyAfterBuy);
            Destroy(this.gameObject);          
        }
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        pannelE.SetActive(false);
    }
    private int RandomIndex()
    {
        return Random.Range(0, weaponsToBuy.Length);
    }

    /*void ChangeWeapon()
    {
        inventory.weapons[Weapon.inventoryIndex] = weaponsToBuy[index];
    }*/

}

