using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    
    public static GameManager instance;
    public int enemyDefeted;
    public int enemyToDefeat;
    [SerializeField]public int points;
    public int point;
    public bool open;
    public GameObject[] maps;
    public GameObject shopMap;
    public GameObject lastMap;
    public int RoomsCleared;
    public int bossDefeted;
    public EconomyPlayerData economyData;
    public Text redCoins;
    public Text yellowCoins;
    public delegate void UpdateUICoins();
    public static event UpdateUICoins updateUICoins;
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
        RoomsCleared = 0;
        enemyDefeted = 0;
    }
    public int GetPoints()
    {
        return points;
    }
    public void ResetPoints()
    {
        points = 0;
    }
    public int CalculatePoints()
    {
        int pointsForRoomCleared = 1000 * RoomsCleared;
        int pointsForEnemyDefeted = 10 * enemyDefeted;
        int bonus = bossDefeted * 10000;
        return pointsForRoomCleared + pointsForEnemyDefeted + bonus;
    }
    public void UpdateCoins()
    {
        updateUICoins();
        /*yellowCoins.text = economyData.coinsYellow.ToString();
        redCoins.text = economyData.coinsRed.ToString();*/
    }

    public void EnablePlayerInput()
    {
        InputManager.instance.EnableInputMapPlayer();
    }

    public GameObject GetPlayer()
    {
        return GameObject.Find("Player");
    }
}
